package com.example.virtusacodingtest.ui.view.detail

import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.virtusacodingtest.BaseActivity
import com.example.virtusacodingtest.databinding.ActivitySchoolDetailBinding
import com.example.virtusacodingtest.models.SchoolDetail
import com.example.virtusacodingtest.ui.viewmodel.SchoolViewModel
import com.example.virtusacodingtest.utils.Constants.EXTRA_SCHOOL_DBN
import dagger.hilt.android.AndroidEntryPoint

/**
 * SchoolDetailActivity - School Detail View
 * @AndroidEntryPoint - Allows to this component can receive dependencies from their respective parent
 */
@AndroidEntryPoint
class SchoolDetailActivity : BaseActivity() {

    private lateinit var binding: ActivitySchoolDetailBinding

    private val adapter: SchoolDetailAdapter by lazy {
        SchoolDetailAdapter(mutableListOf())
    }

    private val schoolViewModel: SchoolViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvSchoolDetails.apply {
            layoutManager = LinearLayoutManager(this@SchoolDetailActivity)
            setHasFixedSize(true)
            adapter = this@SchoolDetailActivity.adapter
        }

        binding.ivBackBtn.setOnClickListener { finish() }

        schoolViewModel.isLoading().observe(this, this::isLoading)
        schoolViewModel.onSchoolDetails().observe(this, this::onSchoolDetails)

        intent.extras?.getString(EXTRA_SCHOOL_DBN)?.let {
            if (isNetworkAvailable()) {
                schoolViewModel.getSchoolDetails(it)
            } else {
                networkUnavailableError()
            }
        } ?: run {
            notFoundError()
        }
    }

    private fun isLoading(isLoading: Boolean) {
        binding.isLoading = isLoading
    }

    private fun onSchoolDetails(schoolDetails: List<SchoolDetail>) {
        if (schoolDetails.isEmpty()) {
            notFoundError()
        } else {
            adapter.update(schoolDetails)
        }
    }
}