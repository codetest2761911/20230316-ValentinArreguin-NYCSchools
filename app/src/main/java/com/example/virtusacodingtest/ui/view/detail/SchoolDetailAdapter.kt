package com.example.virtusacodingtest.ui.view.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.virtusacodingtest.databinding.SchoolDetailsItemBinding
import com.example.virtusacodingtest.models.SchoolDetail

/**
 * Adapter to handle list of Schools details
 */
class SchoolDetailAdapter (private val schoolDetails: MutableList<SchoolDetail>) : RecyclerView.Adapter<SchoolDetailAdapter.SchoolDetailItemViewHolder>() {

    private lateinit var binding: SchoolDetailsItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolDetailItemViewHolder {
        binding = SchoolDetailsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolDetailItemViewHolder()
    }

    override fun getItemCount() = schoolDetails.size

    override fun onBindViewHolder(holder: SchoolDetailItemViewHolder, position: Int) {
        holder.data(schoolDetails[position])
    }

    inner class SchoolDetailItemViewHolder : RecyclerView.ViewHolder(binding.root) {

        fun data(schoolDetail: SchoolDetail) {
            binding.schoolDetail = schoolDetail
        }
    }

    /**
     * SchoolDetailsDiffUtilCallback - Notify updates to the RecyclerView
     */
    inner class SchoolDetailsDiffUtilCallback(
        private val oldList: List<SchoolDetail>,
        private val newList: List<SchoolDetail>) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].dbn == newList[newItemPosition].dbn
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    }

    fun update(newSchoolDetailList: List<SchoolDetail>) {
        val diffResult = DiffUtil.calculateDiff(SchoolDetailsDiffUtilCallback(schoolDetails, newSchoolDetailList))
        schoolDetails.clear()
        schoolDetails.addAll(newSchoolDetailList)
        diffResult.dispatchUpdatesTo(this)
    }
}