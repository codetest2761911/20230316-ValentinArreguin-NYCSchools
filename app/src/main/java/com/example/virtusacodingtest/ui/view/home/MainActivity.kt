package com.example.virtusacodingtest.ui.view.home

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.virtusacodingtest.BaseActivity
import com.example.virtusacodingtest.databinding.ActivityMainBinding
import com.example.virtusacodingtest.models.School
import com.example.virtusacodingtest.ui.view.detail.SchoolDetailActivity
import com.example.virtusacodingtest.ui.viewmodel.SchoolViewModel
import com.example.virtusacodingtest.utils.Constants.EXTRA_SCHOOL_DBN
import dagger.hilt.android.AndroidEntryPoint

/**
 * MainActivity - Schools List View
 * @AndroidEntryPoint - Allows to this component can receive dependencies from their respective parent
 */
@AndroidEntryPoint
class MainActivity : BaseActivity(), SchoolsAdapter.SchoolItemListener {

    private lateinit var binding: ActivityMainBinding

    private val schoolViewModel: SchoolViewModel by viewModels()

    private val adapter: SchoolsAdapter by lazy {
        SchoolsAdapter(mutableListOf(), this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.rvSchools.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            setHasFixedSize(true)
            adapter = this@MainActivity.adapter
        }

        schoolViewModel.isLoading().observe(this, this::isLoading)
        schoolViewModel.onSchools().observe(this, this::onSchools)

        if (isNetworkAvailable()) {
            schoolViewModel.getSchools()
        } else {
            networkUnavailableError()
        }
    }

    override fun onItemClick(dbn: String) {
        val intent = Intent(this, SchoolDetailActivity::class.java).apply {
            putExtra(EXTRA_SCHOOL_DBN, dbn)
        }
        startActivity(intent)
    }

    private fun isLoading(isLoading: Boolean) {
        binding.isLoading = isLoading
    }

    private fun onSchools(schools: List<School>) {
        adapter.update(schools)
    }
}