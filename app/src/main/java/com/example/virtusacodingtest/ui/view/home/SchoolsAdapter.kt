package com.example.virtusacodingtest.ui.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.virtusacodingtest.databinding.SchoolItemBinding
import com.example.virtusacodingtest.models.School

/**
 * Adapter to handle list of Schools
 */
class SchoolsAdapter(private val schools: MutableList<School>, private val listener: SchoolItemListener) : RecyclerView.Adapter<SchoolsAdapter.SchoolItemViewHolder>() {

    private lateinit var binding: SchoolItemBinding

    interface SchoolItemListener {
        fun onItemClick(dbn: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolItemViewHolder {
        binding = SchoolItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolItemViewHolder()
    }

    override fun getItemCount() = schools.size

    override fun onBindViewHolder(holder: SchoolItemViewHolder, position: Int) {
        holder.data(schools[position])
    }

    inner class SchoolItemViewHolder : RecyclerView.ViewHolder(binding.root) {

        fun data(school: School) {
            binding.school = school
            binding.clSchoolItemContainer.setOnClickListener { listener.onItemClick(school.dbn) }
        }
    }

    /**
     * SchoolDetailsDiffUtilCallback - Notify updates to the RecyclerView
     */
    inner class SchoolsDiffUtilCallback(
        private val oldList: List<School>,
        private val newList: List<School>) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].dbn == newList[newItemPosition].dbn
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    }

    fun update(newSchoolList: List<School>) {
        val diffResult = DiffUtil.calculateDiff(SchoolsDiffUtilCallback(schools, newSchoolList))
        schools.clear()
        schools.addAll(newSchoolList)
        diffResult.dispatchUpdatesTo(this)
    }
}