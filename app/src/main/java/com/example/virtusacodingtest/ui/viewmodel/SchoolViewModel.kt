package com.example.virtusacodingtest.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.virtusacodingtest.domain.GetSchoolDetailsUseCase
import com.example.virtusacodingtest.domain.GetSchoolsUseCase
import com.example.virtusacodingtest.models.School
import com.example.virtusacodingtest.models.SchoolDetail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(
    private val getSchoolsUseCase: GetSchoolsUseCase,
    private val getSchoolDetailsUseCase: GetSchoolDetailsUseCase
) : ViewModel() {

    private val isLoading = MutableLiveData<Boolean>()
    private val schools = MutableLiveData<List<School>>()
    private val schoolDetails = MutableLiveData<List<SchoolDetail>>()

    fun getSchools() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getSchoolsUseCase()
            schools.postValue(result)
            isLoading.postValue(false)
        }
    }

    fun getSchoolDetails(dbn: String) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getSchoolDetailsUseCase(dbn)
            schoolDetails.postValue(result)
            isLoading.postValue(false)
        }
    }

    fun isLoading(): LiveData<Boolean> = isLoading
    fun onSchools(): LiveData<List<School>> = schools
    fun onSchoolDetails(): LiveData<List<SchoolDetail>> = schoolDetails
}