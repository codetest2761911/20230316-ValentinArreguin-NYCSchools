package com.example.virtusacodingtest.di

import com.example.virtusacodingtest.data.network.SchoolApiClient
import com.example.virtusacodingtest.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * NetworkModule
 * Provides external dependencies
 * @Module - The modules are the ones that provides the dependencies
 * @InstallIn - Defines the scope of the module(ActivityComponent, ViewModelComponent, SingletonComponent, among others),
 * that is, when a certain component dies, the instances that were created will also die
 */
@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    /**
     * Provide a unique Retrofit instance
     */
    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        val client = OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .build()
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    /**
     * Provide the API client for requests
     */
    @Singleton
    @Provides
    fun provideApiClient(retrofit: Retrofit): SchoolApiClient {
        return retrofit.create(SchoolApiClient::class.java)
    }
}