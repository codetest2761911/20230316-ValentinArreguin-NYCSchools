package com.example.virtusacodingtest.utils

object Constants {

    const val BASE_URL = "https://data.cityofnewyork.us/"
    const val SCHOOL_LIST = "resource/s3k6-pzi2.json"
    const val SCHOOL_DETAILS = "resource/f9bf-2cp4.json"

    const val EXTRA_SCHOOL_DBN = "extra_school_dbn"
}