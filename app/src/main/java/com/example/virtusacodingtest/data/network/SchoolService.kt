package com.example.virtusacodingtest.data.network

import com.example.virtusacodingtest.models.School
import com.example.virtusacodingtest.models.SchoolDetail
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * SchoolService
 * Class to define our services and send the requests
 */
class SchoolService @Inject constructor(private val api: SchoolApiClient) {

    suspend fun getSchools(): List<School> {
        return withContext(Dispatchers.IO) {
            val response = api.getSchools()
            response.body() ?: emptyList()
        }
    }

    suspend fun getSchoolDetails(dbn: String): List<SchoolDetail> {
        return withContext(Dispatchers.IO) {
            val response = api.getSchoolDetails(dbn)
            response.body() ?: emptyList()
        }
    }
}