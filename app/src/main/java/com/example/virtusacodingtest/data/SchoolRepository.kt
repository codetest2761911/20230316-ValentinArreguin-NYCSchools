package com.example.virtusacodingtest.data

import com.example.virtusacodingtest.data.network.SchoolService
import javax.inject.Inject

/**
 * SchoolRepository
 * Class in charge to call the Service
 */
class SchoolRepository @Inject constructor(private val api: SchoolService) {

    suspend fun getSchools() = api.getSchools()

    suspend fun getSchoolDetails(dbn: String) = api.getSchoolDetails(dbn)
}