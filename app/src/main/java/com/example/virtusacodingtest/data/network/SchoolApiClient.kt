package com.example.virtusacodingtest.data.network

import com.example.virtusacodingtest.models.School
import com.example.virtusacodingtest.models.SchoolDetail
import com.example.virtusacodingtest.utils.Constants.SCHOOL_DETAILS
import com.example.virtusacodingtest.utils.Constants.SCHOOL_LIST
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * SchoolApiClient
 * Contains the entry points for requests
 */
interface SchoolApiClient {

    /**
     * Get the list of schools
     */
    @GET(SCHOOL_LIST)
    suspend fun getSchools(): Response<List<School>>

    /**
     * Get the list of school details
     */
    @GET(SCHOOL_DETAILS)
    suspend fun getSchoolDetails(@Query("dbn") dbn: String): Response<List<SchoolDetail>>
}