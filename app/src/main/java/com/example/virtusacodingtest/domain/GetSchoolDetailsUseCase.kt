package com.example.virtusacodingtest.domain

import com.example.virtusacodingtest.data.SchoolRepository
import javax.inject.Inject

/**
 * Use case to call the service to get School Details
 */
class GetSchoolDetailsUseCase @Inject constructor(private val repository: SchoolRepository) {

    suspend operator fun invoke(dbn: String) = repository.getSchoolDetails(dbn)
}