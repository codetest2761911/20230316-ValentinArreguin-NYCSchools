package com.example.virtusacodingtest.domain

import com.example.virtusacodingtest.data.SchoolRepository
import javax.inject.Inject

/**
 * Use case to call the service to get School List
 */
class GetSchoolsUseCase @Inject constructor(private val repository: SchoolRepository) {

    suspend operator fun invoke() = repository.getSchools()
}