package com.example.virtusacodingtest

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.virtusacodingtest.utils.isNetworkAvailable

/**
 * Base class for all the activities
 */
abstract class BaseActivity : AppCompatActivity() {

    protected fun isNetworkAvailable() = applicationContext.isNetworkAvailable()

    protected fun networkUnavailableError() {
        showToast(getString(R.string.error_network_unavailable))
    }

    protected fun notFoundError() {
        showToast(getString(R.string.error_school_detail_extra_not_found))
    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}