package com.example.virtusacodingtest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * VirtusaApplication
 * Main Application class
 * @HiltAndroidApp - triggers Hilt's code generation and other components
 * can access the dependencies that it provides
 */
@HiltAndroidApp
class VirtusaApplication : Application()